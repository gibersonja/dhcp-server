package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
)

var server_port int = 67
var client_port int = 68
var server_ip uint32
var pipe [][]byte
var pool []uint32
var network_bit uint32
var lease_time uint32 = 3600
var rebinding_time uint32 = 3150
var renewal_time uint32 = 1800
var verbose bool = false

func main() {
	if len(os.Args) < 2 {
		er(fmt.Errorf("no arguments given"))
	}

	args := os.Args[1:]
	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" {
			fmt.Print("A.B.C.D/XY\tThe address pool in CIDR notation, with A.B.C.D being the\n")
			fmt.Print("\t\taddress of the DHCP server\n")
			fmt.Printf("--lease=#\tSpecify renewal time (Default: %d)\n", lease_time)
			fmt.Printf("--renew=#\tSpecify renewal time (Default: %d)\n", renewal_time)
			fmt.Printf("--rebind=#\tSpecify renewal time (Default: %d)\n", rebinding_time)
			fmt.Printf("--verbose\tTurn on verbose output (Default: %t)\n", verbose)

			return
		}

		if arg == "--verbose" {
			verbose = true
		}

		regex := regexp.MustCompile(`^(\d+)\.(\d+)\.(\d+)\.(\d+)/(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.Atoi(regex.FindStringSubmatch(arg)[5])
			er(err)

            /* consider dropping this for loop for flipping bits on first... instead
            just flip all bits on (network_bit = 0xFFFFFFFF) then do the following 
            loop as is to just bit shift the int left
            */

			for i := 0; i < tmp; i++ {
				network_bit = network_bit << 1
				network_bit = network_bit + 1
			}
			for i := 0; i < 32-tmp; i++ {
				network_bit = network_bit << 1
			}

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[1])
			er(err)
			server_ip = server_ip + uint32(tmp)
			server_ip = server_ip << 8

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[2])
			er(err)
			server_ip = server_ip + uint32(tmp)
			server_ip = server_ip << 8

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[3])
			er(err)
			server_ip = server_ip + uint32(tmp)
			server_ip = server_ip << 8

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[4])
			er(err)
			server_ip = server_ip + uint32(tmp)

			addresses := get_cidr(server_ip, network_bit)[1:]
			for i := 0; i < len(addresses); i++ {
				if addresses[i] == server_ip {
					pool = addresses[:i]
					pool = append(pool, addresses[i+1:len(addresses)-1]...)
				}
			}
		}

		regex = regexp.MustCompile(`--renew=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 32)
			er(err)
			renewal_time = uint32(tmp)
		}

		regex = regexp.MustCompile(`--lease=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 32)
			er(err)
			lease_time = uint32(tmp)
		}

		regex = regexp.MustCompile(`--rebind=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseUint(regex.FindStringSubmatch(arg)[1], 10, 32)
			er(err)
			rebinding_time = uint32(tmp)
		}

	}

	if verbose {
		fmt.Print("\n\n--- DHCP POOL ---\n\n")
		for i := 0; i < len(pool); i++ {
			fmt.Printf("%d.%d.%d.%d\n", byte(pool[i]>>24), byte(pool[i]>>16), byte(pool[i]>>8), byte(pool[i]))
		}

		fmt.Print("\n\n--- TIMES ---\n\n")
		fmt.Printf("LEASE TIME:  %d seconds\n", lease_time)
		fmt.Printf("RENEW TIME:  %d seconds\n", renewal_time)
		fmt.Printf("REBIND TIME: %d seconds\n", rebinding_time)

		fmt.Print("\n\n--- PORTS ---\n\n")
		fmt.Printf("CLIENT PORT: %d\n", client_port)
		fmt.Printf("SERVER PORT: %d\n", server_port)
	}

	if server_ip == 0 || len(pool) == 0 {
		er(fmt.Errorf("must specify CIDR range"))
	}

	server, err := net.ListenPacket("udp", fmt.Sprintf(":%d", server_port))
	er(err)

	for {
		buf := make([]byte, 600)
		_, _, err := server.ReadFrom(buf)
		er(err)

		go parse_buf(buf)
		go process(pipe, server)
	}
}

func process(pipe [][]byte, server net.PacketConn) {
	for i := 0; i < len(pipe); i++ {
		buf := pipe[i]
		for i := 240; i < len(buf); i++ {
			if buf[i] == 53 && i+2 < len(buf) {
				/*
				   Value   Message Type
				            -----   ------------
				              1     DHCPDISCOVER
				              2     DHCPOFFER
				              3     DHCPREQUEST
				              4     DHCPDECLINE
				              5     DHCPACK
				              6     DHCPNAK
				              7     DHCPRELEASE

				     Code   Len  Type
				    +-----+-----+-----+
				    |  53 |  1  | 1-7 |
				    +-----+-----+-----+
				*/

				if buf[i+1] == 1 && buf[i+2] == 1 { // DHCP DISCOVER
					if verbose {
						fmt.Print("\n\n--- RECIEVED DHCP-DISCOVER ---\n")
						fmt.Printf("\nXID: 0x%02X%02X%02X%02X\n", buf[4], buf[5], buf[6], buf[7])
					}
					dhcpOffer(buf, server)
				}

				if buf[i+1] == 1 && buf[i+2] == 3 { // DHCP REQUEST
					if verbose {
						fmt.Print("\n\n--- RECIEVED DHCP-REQUET ---\n")
						fmt.Printf("\nXID: 0x%02X%02X%02X%02X\n", buf[4], buf[5], buf[6], buf[7])
					}
					dhcpAck(buf, server)
				}

				if buf[i+1] == 1 && buf[i+2] == 7 { // DHCP RELEASE

					var client_ip uint32

					client_ip = uint32(buf[12])
					client_ip = client_ip << 8
					client_ip = client_ip + uint32(buf[13])
					client_ip = client_ip << 8
					client_ip = client_ip + uint32(buf[14])
					client_ip = client_ip << 8
					client_ip = client_ip + uint32(buf[15])

					if verbose {
						fmt.Print("\n\n--- RECIEVED DHCP-RELEASE ---\n")
						fmt.Printf("\nXID: 0x%02X%02X%02X%02X\n", buf[4], buf[5], buf[6], buf[7])
						fmt.Printf("Adding IP %d.%d.%d.%d back to pool\n", byte(client_ip>>24), byte(client_ip>>16), byte(client_ip>>8), byte(client_ip))
					}

					pool = append(pool, client_ip)
				}
			}
		}
	}
	// Clear buffer

	pipe = nil
}

func dhcpAck(buf []byte, server net.PacketConn) {
	// Check requested IP
	var request_ip uint32
	for i := 240; i < len(buf); i++ {
		if buf[i] == 50 && i+5 < len(buf) {
			if buf[i+1] == 4 {
				request_ip = uint32(buf[i+2])
				request_ip = request_ip << 8
				request_ip = request_ip + uint32(buf[i+3])
				request_ip = request_ip << 8
				request_ip = request_ip + uint32(buf[i+4])
				request_ip = request_ip << 8
				request_ip = request_ip + uint32(buf[i+5])
				break
			}
		}
	}

	if verbose {
		fmt.Printf("Client requests IP: %d.%d.%d.%d\n", byte(request_ip>>24), byte(request_ip>>16), byte(request_ip>>8), byte(request_ip))
	}

	var client_ip uint32

	for i := 0; i < len(pool); i++ {
		if request_ip == pool[i] {
			client_ip = request_ip

			// Remove IP from pool

			a := pool[:i]
			b := pool[i+1:]

			pool = a
			pool = append(pool, b...)

			fmt.Printf("IP %d.%d.%d.%d is available\n", byte(client_ip>>24), byte(client_ip>>16), byte(client_ip>>8), byte(client_ip))
			break
		}
	}

	if client_ip != 0 {
		// Clear options
		outbuf := buf[:240]

		outbuf[0] = 2 // Message Boot Reply

		outbuf[16] = byte(client_ip >> 24)
		outbuf[17] = byte(client_ip >> 16) // Set yiaddr
		outbuf[18] = byte(client_ip >> 8)
		outbuf[19] = byte(client_ip)

		outbuf = append(outbuf, 53, 1, 5)                                                                                                 // DHCP ACK
		outbuf = append(outbuf, 1, 4, byte(network_bit>>24), byte(network_bit>>16), byte(network_bit>>8), byte(network_bit))              // SUBNET MASK
		outbuf = append(outbuf, 51, 4, byte(lease_time>>24), byte(lease_time>>16), byte(lease_time>>8), byte(lease_time))                 // LEASE TIME
		outbuf = append(outbuf, 58, 4, byte(renewal_time>>24), byte(renewal_time>>16), byte(renewal_time>>8), byte(renewal_time))         // RENEWAL TIME
		outbuf = append(outbuf, 59, 4, byte(rebinding_time>>24), byte(rebinding_time>>16), byte(rebinding_time>>8), byte(rebinding_time)) // REBINDING TIME
		outbuf = append(outbuf, 54, 4, byte(server_ip>>24), byte(server_ip>>16), byte(server_ip>>8), byte(server_ip))                     // SERVER IDENTIFIER
		outbuf = append(outbuf, 255)                                                                                                      // END

		// Message should be padded with NULL bytes to the nearest 16-bit WORD boundary... i.e. should be a even number of bytes
		if len(outbuf)%2 != 0 {
			outbuf = append(buf, 0)
		}

		// Sending DHCP ACK

		client, err := net.ResolveUDPAddr("udp", fmt.Sprintf("%d.%d.%d.%d:%d", byte(client_ip>>24), byte(client_ip>>16), byte(client_ip>>8), byte(client_ip), client_port))
		er(err)

		if verbose {
			fmt.Print("\n\n--- SENDING DHCP-ACK ---\n")
			fmt.Printf("\nXID: 0x%02X%02X%02X%02X\n", outbuf[4], outbuf[5], outbuf[6], outbuf[7])
			fmt.Printf("\nACKNOWLEDGE IP: %d.%d.%d.%d\n", byte(client_ip>>24), byte(client_ip>>16), byte(client_ip>>8), byte(client_ip))
		}
		server.WriteTo(outbuf, client)

		return
	}
}

func dhcpOffer(buf []byte, server net.PacketConn) {
	var client_ip uint32 = pool[0]

	// Clear options
	outbuf := buf[:240]

	outbuf[0] = 2 // Message Boot Reply

	outbuf[16] = byte(client_ip >> 24)
	outbuf[17] = byte(client_ip >> 16) // Set yiaddr
	outbuf[18] = byte(client_ip >> 8)
	outbuf[19] = byte(client_ip)

	outbuf[20] = byte(server_ip >> 24)
	outbuf[21] = byte(server_ip >> 16) // Set siaddr
	outbuf[22] = byte(server_ip >> 8)
	outbuf[23] = byte(server_ip)

	outbuf = append(outbuf, 53, 1, 2)                                                                                                 // DHCP OFFER
	outbuf = append(outbuf, 1, 4, byte(network_bit>>24), byte(network_bit>>16), byte(network_bit>>8), byte(network_bit))              // SUBNET MASK
	outbuf = append(outbuf, 51, 4, byte(lease_time>>24), byte(lease_time>>16), byte(lease_time>>8), byte(lease_time))                 // LEASE TIME
	outbuf = append(outbuf, 58, 4, byte(renewal_time>>24), byte(renewal_time>>16), byte(renewal_time>>8), byte(renewal_time))         // RENEWAL TIME
	outbuf = append(outbuf, 59, 4, byte(rebinding_time>>24), byte(rebinding_time>>16), byte(rebinding_time>>8), byte(rebinding_time)) // REBINDING TIME
	outbuf = append(outbuf, 54, 4, byte(server_ip>>24), byte(server_ip>>16), byte(server_ip>>8), byte(server_ip))                     // SERVER IDENTIFIER
	outbuf = append(outbuf, 255)                                                                                                      // END

	// Message should be padded with NULL bytes to the nearest 16-bit WORD boundary... i.e. should be a even number of bytes
	if len(outbuf)%2 != 0 {
		outbuf = append(outbuf, 0)
	}

	// Sending DHCP OFFER

	client, err := net.ResolveUDPAddr("udp", fmt.Sprintf("%d.%d.%d.%d:%d", byte(client_ip>>24), byte(client_ip>>16), byte(client_ip>>8), byte(client_ip), client_port))
	er(err)

	if verbose {
		fmt.Print("\n\n--- SENDING DHCP-OFFER ---\n")
		fmt.Printf("\nXID: 0x%02X%02X%02X%02X\n", outbuf[4], outbuf[5], outbuf[6], outbuf[7])
		fmt.Printf("\nOFFER IP: %d.%d.%d.%d\n", byte(client_ip>>24), byte(client_ip>>16), byte(client_ip>>8), byte(client_ip))

	}
	server.WriteTo(outbuf, client)
}

func parse_buf(buf []byte) {
	if verbose {
		fmt.Print("\n\n--- INCOMING UDP DATA ---\n\n")

	}
	for i := 240; i < len(buf); i++ { // 240 = begining of options section (236) + magic cookie (1st 4 Bytes)
		if buf[i] == 53 && i+2 < len(buf) {
			if buf[i+1] == 1 {
				if buf[i+2] > 0 && buf[i+2] < 8 { // DHCP MESSAGE
					pipe = append(pipe, buf)
					return
				}
			}
		}
	}
}

func get_cidr(ip uint32, subnetmask uint32) []uint32 {
	first_ip := ip & subnetmask
	last_ip := ip | ^subnetmask

	var ips []uint32

	for i := first_ip; i <= last_ip; i++ {
		ips = append(ips, i)
	}

	return ips
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
